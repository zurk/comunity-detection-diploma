from random import *

import os
import subprocess
from itertools import repeat
from .Extends import *
from . import casheResult
from numpy.random import permutation
import cPickle

def lpartition(groupsCount, vertInGroup, pin, pout):
    """
    Generate simple l-partition benchmark
    :param groupsCount: communities number
    :param vertInGroup: vertex count in each group
    :param pin: edge creation probability inside community
    :param pout: edge creation probability outside community
    :return: VertexClustering object from igraph lib
    """

    G = ig.GraphBase.disjoint_union(ig.Graph(), [ig.GraphBase.Erdos_Renyi(vertInGroup, pin) for i in xrange(groupsCount)])
    for vert in G.vs:
        for n in (x.index for x in G.vs if x.index / vertInGroup != vert.index / vertInGroup and not G.are_connected(vert.index, x.index)):
            if random() <= pout:
                G.add_edge(vert.index, n)
    return ig.VertexClustering(G, [y for x in xrange(groupsCount) for y in repeat(x, vertInGroup)])

def testFabric(GVC, padd, count):
    """
    add new edges outside community to graph
    :param GVC: Graph like VertexClustering object from igraph lib
    :param padd: edge creation probability outside community
    :param count: count of edges addition
    :return: new Graph like VertexClustering object from igraph lib
    """
    yield ig.VertexClustering(GVC.graph.copy(), GVC.membership)
    edges = []

    for vert in GVC.graph.vs:
        edges.extend([(vert.index, x) for x in xrange(vert.index, len(GVC.graph.vs)) if GVC.membership[x] != GVC.membership[vert.index] and not GVC.graph.are_connected(vert.index, x)])
    edges = permutation(edges)
    edgesStepCount = int(len(edges) * padd)

    k = 0
    # for e in edges:
    #     if not GVC.graph.are_connected(e[0], e[1]):
    #         GVC.graph.add_edge(e[0], e[1])
    #         k += 1
    #         if(k%edgesStepCount == 0):
    #             print '.',
    #             yield ig.VertexClustering(GVC.graph.copy(), GVC.membership)
    #             if(k / edgesStepCount == count ):
    #                 print 'OK! '
    #                 break
    for k in xrange(count):
        GVC.graph.add_edges(edges[k*edgesStepCount:(k+1)*edgesStepCount])
        print '.',
        yield ig.VertexClustering(GVC.graph.copy(), GVC.membership)
    print 'OK! '

def Lancichinetti(nodesCount=1000, averageDeg=40, maxDeg=100, degDistrExp=2, commSizeDistr=2, commSizeMin=30, commSizeMax=100, mixing=0.02):
    """
    Benchmark graphs for testing community detection algorithms from
    Andrea Lancichinetti, Santo Fortunato and Filippo Radicchi1
    http://arxiv.org/pdf/0805.4770.pdf

    This function is wrapper for exe file.
    In future it should be rewritten by Python-C API

    :param nodesCount:  number of nodes
    :param averageDeg:  average degree
    :param maxDeg: maximum degree
    :param degDistrExp: exponent for the degree distribution
    :param commSizeDistr:  exponent for the community size distribution
    :param commSizeMax: maximum for the community sizes
    :param commSizeMin: minimum for the community sizes
    :param mixing: mixing parameter
    :return:
    """

    cwd = os.getcwd()
    os.chdir('./CommunityDetection/Lancichinetti')

    f = open("parameters.dat", 'w')
    f.write('\n'.join(str(p) for p in (nodesCount, averageDeg, maxDeg, degDistrExp, commSizeDistr, mixing, commSizeMin, commSizeMax)))
    f.close()
    devnull = open(os.devnull, 'wb') #python >= 2.4
    p = subprocess.Popen('Lancichinetti.exe', stdout=devnull, stderr=devnull)
    p.wait()

    with open("network.dat") as nw:
         edges = [[int(x)-1 for x in line.split()] for line in nw]
    G = ig.Graph(edges[-1][0], edges)
    with open("community.dat") as nw:
         comm = [int(line.split()[1]) for line in nw]

    os.chdir(cwd)

    return ig.VertexClustering(ig.Graph.simplify(G), comm)

def GirvanNewman(mixing=0.2):
    return Lancichinetti(128, 16, 16, 1, 1, 32, 32, mixing)

def read_nodeadjlist(filename):
    vs = []
    edges = []
    G = ig.Graph()
    for line in open(filename):
        e1, es = line.split(':')
        vs.append(e1)
        edges += [(e, e1) for e in es.split() if e != e1]
    G.add_vertices(vs)
    G.add_edges(edges)
    return G.simplify()

def ego_data():
    path = '../data/egonets/'
    return [read_nodeadjlist(path + file) for file in os.listdir(path) if file.endswith(".egonet")]

def read_ego_dict(filename):

    with file(filename, 'rb') as f:
        d = cPickle.load(f)
    G = ig.Graph()
    G.add_vertices([str(x) for x in d.keys()])
    print '{}: {}'.format(filename, len(d))
    all_vert = set(d.keys())
    for key in d:
        d[key] = set(d[key]) & all_vert
    k = 0
    v1 = []
    [ v1.extend((str(key),)*len(d[key])) for key in d]
    v2 = [str(x) for key in d for x in d[key]]
    G.add_edges(zip(v1,v2))
    return ig.Graph.simplify(G)

@casheResult
def ego_vk_data(pool):
    path = '../data/vk-egonets/'
    return [x for x in pool.map( read_ego_dict, [(path + file) for file in os.listdir(path) if file.endswith(".ego")]) if x != None]

def internet_data():
    path = '../data/internet_as-22july06.gml'
    return ig.read(path)

def MSU_overheard_data():
    path = '../data/54295855.GroupGraph'
    return read_ego_dict(path)