import networkx as nx
import igraph as ig

def convertPartition(P):
    if (isinstance(P, dict)):
        Pnew = dict()
        for key in P:
            if isinstance(P[key], list):
                for v in P[key]:
                    Pnew[v] = key
            else:
                if P[key] in Pnew:
                    Pnew[P[key]].append(key)
                else:
                    Pnew[P[key]] = [key]
    elif (isinstance(P, list)):
        Pnew = dict()
        for v in xrange(len(P)):
            if P[v] in Pnew:
                Pnew[P[v]].append(v)
            else:
                Pnew[P[v]] = [v]
    return Pnew


def nx2ig(G):
    Gnew = ig.Graph()
    Gnew.add_vertices(G.nodes())
    Gnew.add_edges(G.edges())
    return Gnew


def ig2nx(G):
    Gnew = nx.Graph()
    Gnew.add_nodes_from(v.index for v in G.vs)
    Gnew.add_edges_from(e.tuple for e in G.es)
    return Gnew


def nx2igPrtt(G, prtt):
    return ig.VertexClustering(G, prtt.values())
