import igraph as ig


def modularity(G):
    return G.modularity


def compareCommunities(G1, G2, methods=('split-join', '1-nmi')):
    """
    :param G1:
    :param G2:
    :param method:
        method - the measure to use.
        "vi" or "meila" means the variation of information metric of Meila (2003),
        "nmi" or "danon" means the normalized mutual information as defined by Danon et al (2005),
        "split-join" means the split-join distance of van Dongen (2000),
        "rand" means the Rand index of Rand (1971),
        "adjusted_rand" means the adjusted Rand index of Hubert and Arabie (1985).
    :return:
    """
    return tuple([ig.compare_communities(G1, G2, m) if m != '1-nmi' else 1 - ig.compare_communities(G1, G2, 'nmi') for m in methods])