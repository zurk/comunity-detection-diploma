"""
Methods info see: http://igraph.org/python/doc/igraph.Graph-class.html

"""

import community
import igraph as ig
from .Extends import ig2nx, nx2igPrtt, convertPartition
from . import createMetaGraph
from sklearn.cluster import *
from sklearn.preprocessing import MultiLabelBinarizer, OneHotEncoder
import numpy as np

methodsList = {
    'infomap': lambda G: G.community_infomap(),
    'fastgreedy': lambda G: G.community_fastgreedy().as_clustering(),
    'eigenvector': lambda G: G.community_leading_eigenvector(),
    #'labelProp': lambda G: G.community_label_propagation(),
    'multilevel': lambda G: G.community_multilevel(),
    #'optModularity': lambda G: G.community_optimal_modularity(),
    #'betweenness': lambda G: G.community_edge_betweenness().as_clustering(),
    'walktrap': lambda G: G.community_walktrap().as_clustering(),
    #'spinglass': lambda G: G.commu nity_spinglass(),
}

AgrMethods = {
    'aggAggClust': lambda VCs: AgglomerativeClusteringAgg(VCs),
    'aggKMeansClust': lambda VCs: ClusteringAgg(VCs, KMeans),
    #'aggAffPropClust': lambda VCs: ClusteringAgg(VCs, AffinityPropagation),
    #'aggMeanShiftClust': lambda VCs: ClusteringAgg(VCs, MeanShift),
    #'aggSpectClust': lambda VCs: ClusteringAgg(VCs, SpectralClustering),
    #'aggWardClust': lambda VCs: AgglomerativeClusteringWardAgg(VCs),

    'aggMultilevel': lambda VCs: multilevelAgr(VCs),
    'aggFastgreedy': lambda VCs: fastgreedyAgr(VCs),
    #'aggWalktrap': lambda VCs: walktrapAgr(VCs),
    #'aggOptModularity': lambda VCs: optModularityAgr(VCs),
}


def AgglomerativeClusteringWardAgg(VCs):
    X = np.array([x.membership for x in VCs if len(x) > 1]).transpose()
    clustNo = np.median(np.array([len(vc) for vc in VCs]))
    clust = AgglomerativeClustering(n_clusters=int(clustNo), affinity="euclidean", linkage="ward")
    lb = OneHotEncoder()
    lb.fit(X)
    Xtr = lb.transform(X).toarray()
    y = clust.fit_predict(Xtr)
    return ig.VertexClustering(VCs[0].graph, y)

def AgglomerativeClusteringAgg(VCs):
    X = np.array([x.membership for x in VCs if len(x) > 1]).transpose()
    clustNo = np.median(np.array([len(vc) for vc in VCs]))
    clust = AgglomerativeClustering(n_clusters=int(clustNo), affinity="euclidean", linkage="complete")
    lb = OneHotEncoder()
    lb.fit(X)
    Xtr = lb.transform(X).toarray()
    y = clust.fit_predict(Xtr)
    return ig.VertexClustering(VCs[0].graph, y)

def ClusteringAgg(VCs, meth):
    X = np.array([x.membership for x in VCs if len(x) > 1]).transpose()
    clustNo = np.median(np.array([len(vc) for vc in VCs]))
    try:
        clust = meth(n_clusters=int(clustNo))
    except:
        clust = meth()
    lb = OneHotEncoder()
    lb.fit(X)
    Xtr = lb.transform(X).toarray()
    y = clust.fit_predict(Xtr)
    return ig.VertexClustering(VCs[0].graph, y)



def multilevelAgr(VCs):
    membershipNew = zip(*[vc.membership for vc in VCs])
    labelsNew = list(set(membershipNew))
    membershipNew = [labelsNew.index(x) for x in membershipNew]  # crosspartition
    metaG = createMetaGraph(VCs[0].graph, membershipNew)
    VC = metaG.community_multilevel(metaG.es["weight"])
    #print len(labelsNew),
    return ig.VertexClustering(VCs[0].graph, [VC.membership[v] for v in membershipNew])


def fastgreedyAgr(VCs):
    membershipNew = zip(*[vc.membership for vc in VCs])
    labelsNew = list(set(membershipNew))
    membershipNew = [labelsNew.index(x) for x in membershipNew]  # crosspartition
    metaG = createMetaGraph(VCs[0].graph, membershipNew)
    VC = metaG.community_fastgreedy(weights=metaG.es["weight"]).as_clustering()
    #print '{}-{}'.format(len(labelsNew),len(set(VC.membership))),
    return ig.VertexClustering(VCs[0].graph, [VC.membership[v] for v in membershipNew])

def optModularityAgr(VCs):
    membershipNew = zip(*[vc.membership for vc in VCs])
    labelsNew = list(set(membershipNew))
    membershipNew = [labelsNew.index(x) for x in membershipNew]  # crosspartition
    metaG = createMetaGraph(VCs[0].graph, membershipNew)
    VC = metaG.community_optimal_modularity(weights=metaG.es["weight"])
    #print '{}-{}'.format(len(labelsNew),len(set(VC.membership))),
    return ig.VertexClustering(VCs[0].graph, [VC.membership[v] for v in membershipNew])

def walktrapAgr(VCs):
    membershipNew = zip(*[vc.membership for vc in VCs])
    labelsNew = list(set(membershipNew))
    membershipNew = [labelsNew.index(x) for x in membershipNew]  # crosspartition
    metaG = createMetaGraph(VCs[0].graph, membershipNew)
    VC = metaG.community_walktrap(weights=metaG.es["weight"]).as_clustering()
    #print '{}-{}'.format(len(labelsNew),len(set(VC.membership))),
    return ig.VertexClustering(VCs[0].graph, [VC.membership[v] for v in membershipNew])