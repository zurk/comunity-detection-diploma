from __future__ import division
from colorsys import hsv_to_rgb
import importlib
import igraph as ig

from matplotlib import pyplot
from os.path import isfile
from cPickle import dump, load
from os import listdir, path
from inspect import getsourcelines
import hashlib
from datetime import datetime

__all__ = ["Benchmarks", "Measures", "Methods", "AgrMethods", "get_color", "createMetaGraph", 'drawTests', 'draw_last_test', 'draw_res', 'draw_res_no_vcs', 'casheResult', 'getCashe']

def get_color(count):
    f = []
    for i in range(count):
        f.append("#{0:02x}{1:02x}{2:02x}".format(*[int(x) for x in hsv_to_rgb(1. * i / count, 1, 105 + 150 * (i % 4 + 1)/4)]))
    return f


def drawTests(VCs, show=True):
    from datetime import datetime
    time = str(datetime.now())[5:-10].replace(':', '-')
    for i in xrange(len(VCs)):
        VCs[i].es["color"] = "#00000020"
        ig.plot(VCs[i], vertex_size=7, target='../plots/ego_vk/graph{}__{}.png'.format(i, time), bbox=(0, 0, 2000, 2000), vertex_frame_width=0.3, inline=not show, layout=None)

# def drawTests(VCs):
#     from .Extends import ig2nx
#     import networkx as nx
#     import matplotlib.pyplot as plt
#     from datetime import datetime
#
#     time = str(datetime.no w())[5:-10].replace(':', '-')
#     Gn = ig2nx(VCs[0].graph)
#     prtt = VCs[0].membership
#     col = get_color(max(prtt)+1)
#     pos = nx.graphviz_layout(Gn)
#
#     for i in xrange(len(VCs)):
#         fig = plt.figure(frameon=False)
#         fig.set_size_inches(5, 8)
#         Gn = ig2nx(VCs[i].graph)
#         ax = plt.Axes(fig, [0., 0., 1., 1.], )
#         ax.set_axis_off()
#         fig.add_axes(ax)
#
#         nx.draw_networkx_nodes(Gn, pos, node_color='b', node_size=120, alpha=0.8)
#         nx.draw_networkx_edges(Gn, pos, width=1, alpha=0.4, edge_color='k')
#         [nx.draw_networkx_nodes(Gn, pos, nodelist=[v], node_color=col[prtt[v]], node_size=90, alpha=0.7) for v in Gn.nodes_iter()]
#         name = '../plots/graph{}__{}.png'.format(i, time)
#         plt.savefig(name, dpi=300)

def createMetaGraph(G, membership):
    """
    Create metagraph by  aggregation vertexes from same community.
    :param G:
    :param vertClustering:
    :return: Weighted Graph, where vertex is communities
    """
    v = list(set(membership))
    metaG = ig.Graph(len(v))
    metaG.es["weight"] = 1.0
    for e in G.es:
        metaG[membership[e.source], membership[e.target]] += 1
        metaG[membership[e.target], membership[e.source]] += 1
    return metaG

Methods = importlib.import_module(".Methods", "CommunityDetection").methodsList
AgrMethods = importlib.import_module(".Methods", "CommunityDetection").AgrMethods



def draw_last_test():
    t = getCashe("genTests")
    if t:
        with file(t) as f:
            tests = load(f)
        drawTests(tests)

def draw_res():
    (res, start, internal, step, count) = load(file('./result/temp', 'r'))
    yls = ('split-join distance', 'Normalized Mutual Information')
    pyplot.rc('axes', color_cycle=get_color(len(res)))
    for i in xrange(2):

        fig, ax = pyplot.subplots()
        pyplot.hold('on')
        pyplot.grid('on')
        pyplot.xlabel('external link probability')
        pyplot.ylabel(yls[i])
        [pyplot.plot([start+step*(x+1) for x in xrange(count)], [res[k][x][1][i] for x in xrange(count)], label=k, linewidth=2.0) for k in res]
        if(i == 0):
            ax.legend(loc='upper left', fontsize=12)
        else:
            ax.legend(loc='lower right', fontsize=12)

    pyplot.show()

def draw_res_no_vcs():
    (res, start, internal, step, count) = load(file('./result/temp', 'r'))
    yls = ('split-join distance', '1 - Normalized Mutual Information')
    pyplot.rc('axes', color_cycle=get_color(len(res)))
    for i in xrange(2):

        fig, ax = pyplot.subplots()
        pyplot.hold('on')
        pyplot.grid('on')
        pyplot.xlabel('external link probability')
        pyplot.ylabel(yls[i])
        [pyplot.plot([start+step*(x+1) for x in xrange(count)], [res[k][x][i] for x in xrange(count)], label=k, linewidth=2.0) for k in res if k != 'aggWalktrap']
        if(i == 0):
            ax.legend(loc='upper left', fontsize=9)
        else:
            ax.legend(loc='lower right', fontsize=9)

    pyplot.show()


def getCashe(name):
    cashe = ['./result/' + f for f in listdir('./result') if f.endswith('.' + name)]
    return max(cashe) if cashe else cashe


def casheResult(fun):
    def casheFun(*args, **kwargs):
        cashe = getCashe(fun.__name__)
        md5 = hashlib.md5()
        if cashe and path.isfile(cashe + '.codehash'):
            with file(cashe + '.codehash') as f:
                ok_hash = f.readline() == hashlib.sha224(''.join(getsourcelines(fun)[0]) + str(args) + str(kwargs)).hexdigest()
        if cashe: #and ok_hash:
            with file(cashe) as f:
                print 'loaded from cashe file {}'.format(cashe)
                return load(f)

        res = fun(*args, **kwargs)
        name = './result/{0}.'.format(str(datetime.now())[5:-10].replace(':', '-')) + fun.__name__
        with file(name, 'w') as f:
            dump(res, f)
        with file(name + '.codehash', 'w') as f:
            f.write(str(hashlib.sha224(''.join(getsourcelines(fun)[0]) + str(args) + str(kwargs)).hexdigest()))
        return res
    return casheFun
