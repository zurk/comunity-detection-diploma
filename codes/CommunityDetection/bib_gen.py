__author__ = 'zurk'
#!/usr/bin/env python

# get the arxiv id
import sys
from string import strip, split


items = ['0805.4770v4', '0908.1062', '0605087', '0709.2938v1', '0906.0612', '0803.0476', '0707.0609', 'http://arxiv.org/abs/cond-mat/0408187',  ]
s = ''
for arg in items:
    arg = strip(arg)
    arg = strip(arg, chars="arxiv:")
    arg = strip(arg, chars="http://")
    arg = strip(arg, chars="www.")
    arg = strip(arg, chars="arxiv.org/abs/")
    arg = strip(arg, chars="arxiv.org/pdf/")
    arg = split(arg, sep='v')[0]
    xid = strip(arg)

    # download the xml
    import urllib
    from xml.dom import minidom
    usock = urllib.urlopen('http://export.arxiv.org/api/query?id_list='+xid)
    xmldoc = minidom.parse(usock)
    usock.close()

    d = xmldoc.getElementsByTagName("entry")[0]
    try:
        date = d.getElementsByTagName("updated")[0].firstChild.data
        text_year = date[:4]
    except:
        text_year = '2005'

    title = d.getElementsByTagName("title")[0]
    text_title = title.firstChild.data#.encode('ascii', 'ignore')

    authorlist = []
    first = True
    for person_name in d.getElementsByTagName("author"):
        # get names
        name = person_name.getElementsByTagName("name")[0]
        text_name = name.firstChild.data#.encode('ascii', 'ignore')
        text_given_name = ' '.join(text_name.split()[:-1])
        text_surname = text_name.split()[-1]
        authorlist.append(text_surname+", "+text_given_name)
        #first author?
        if first:
            text_first_author_surname = text_surname
            first = False

    # output
    s += "@MISC{"+text_first_author_surname+text_year[-2:]+",\n"
    s += "author = {"+" and ".join(authorlist)+"},\n"
    s += "title = {"+text_title+"},\n"
    s += "year = {"+text_year+"},\n"
    s += "eprint = {"+xid+"},\n"
    s += "URL = {http://www.arxiv.org/abs/"+xid+"},\n"
    s += "}\n\n"

with file('bibl.bib', 'w') as f:
    f.write(s)
print s