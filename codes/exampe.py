from __future__ import division

from CommunityDetection import *
from cPickle import dump, load
import multiprocessing
import numpy as np
import time

from collections import OrderedDict


def workerMethods(arg):
    (name, VC) = arg
    VCnew = Methods[name](VC.graph)
    t = Measures.compareCommunities(VC, VCnew)
    print 'w',

    return (VCnew, t)

def workerAgrMethods(arg):
    (name, ans, VCs) = arg
    VCnew = AgrMethods[name](VCs)
    t = Measures.compareCommunities(ans, VCnew)
    return (VCnew, t)

#@casheResult
def genTests(start, internal, step, count):
    return [x for x in Benchmarks.testFabric(Benchmarks.Lancichinetti(mixing=start), step, count)]

def genTests2(start, internal, step, count):
    return [Benchmarks.GirvanNewman(mixing=x)for x in np.linspace(start, start+step*count, count)]

#@casheResult
def calcDist(tests):
    res = OrderedDict()
    for name in Methods:
        wkr = 0
        print 'start: {}'.format(name),
        res[name] = pool.map(workerMethods, ((name, test) for test in tests))
        print ' OK!'
    print '!'
    res2 = res
    for name in AgrMethods:
        res2[name] = pool.map(workerAgrMethods, ((name, tests[i], [res[key][i][0] for key in res]) for i in range(len(tests))))
        print name,
    print '!'
    return res2

if __name__ == '__main__':
    import igraph as ig
    g= ig.Graph()
    g.add_vertices(8)
    g.add_edges(((1,2),(1,3),(2,3),(3,4),(4,5),(4,6), (5,6), (5,7), (6,7), (6,8), (7,8)))

    # pool = multiprocessing.Pool(multiprocessing.cpu_count())
    # start = 0.01
    # internal = 0.5
    # step = 0.02
    # count = 20
    #
    # repCount = 100
    # mean_res = {}
    # res = []
    # start_time = time.time()
    # for i in xrange(repCount):
    #     elapsed_time = np.floor(time.time() - start_time)
    #
    #     if i != 0:
    #         print '~~~ Iteration {}/{} {}/{} sec ~~~'.format(i, repCount, elapsed_time, elapsed_time / i * repCount)
    #     else:
    #         print '~~~ Iteration {}/{} ~~~'
    #     tests = genTests(start, internal, step, count)
    #     res_t = calcDist(tests)
    #     res.append({key: [np.array(rk[1]) for rk in res_t[key]] for key in res_t})
    #
    # n = len(res_t.values()[0])
    # mean_res = {key: np.array([(0, 0) for i in xrange(n)], dtype='float64') for key in res_t}
    # for res_t in res:
    #     for key in res_t:
    #         for i in xrange(len(res_t[key])):
    #             for j in xrange(len(res_t[key][i])):
    #                 mean_res[key][i][j] += res_t[key][i][j]
    # for key in res_t:
    #     mean_res[key] /= repCount
    # print mean_res
    #
    # with file('./result/temp', 'w') as f:
    #     dump((mean_res, start, internal, step, count), f)
    draw_res_no_vcs()
    #draw_last_test()