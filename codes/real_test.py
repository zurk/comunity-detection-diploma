from __future__ import division

from CommunityDetection import *
from cPickle import dump, load
import multiprocessing
import numpy as np

from matplotlib import pyplot

from collections import OrderedDict


def workerMethods(arg):
    (name, graph) = arg
    VCnew = Methods[name](graph)
    t = Measures.modularity(VCnew)
    print ' w {}'.format(name),

    return (VCnew, t)

def workerAgrMethods(arg):
    (name, VCs) = arg
    VCnew = AgrMethods[name](VCs)
    t = Measures.modularity(VCnew)
    print 'w {}'.format(name),

    return (VCnew, t)

#@casheResult
def calcDist(Data):
    res = OrderedDict()
    if(type(Data) is list):
        for name in Methods:
            print 'start: {}'.format(name),
            res[name] = map(workerMethods, ((name, d) for d in Data if len(d.es) > 10))
            print ' OK!'
        print '!'
        res2 = res
        for name in AgrMethods:
            res2[name] = pool.map(workerAgrMethods, ((name, [res[key][i][0] for key in res]) for i in xrange(len(res.values()[0]))))
            print name,
        print '!'
        return res2
    else:
        print 'start...'
        t = pool.map(workerMethods, ((name, Data) for name in Methods))
        res = OrderedDict(zip(Methods.keys(), t))
        print ' OK!'
        print '!'
        res2 = res
        tt = pool.map(workerAgrMethods, ((name, [res[key][0] for key in res]) for name in AgrMethods))
        t = OrderedDict(zip(AgrMethods.keys(), tt))
        for key in t:
            res2[key] = t[key]
        print '!'
        return res2


def draw_real_res():
    res = load(file('./result/temp.real', 'rb'))
    n = len(res)
    col = get_color(n)
    k=4

    mat = [[res[key][i][1] for i in xrange(len(res[key]))] for key in res if key in Methods]
    best = np.array(mat).max(0)

    for key in res:
        if key in AgrMethods:
            VCs, mod = zip(*res[key])
            X = np.array(mod) - best

            pyplot.scatter(np.array([k]*len(mod)) + 0.6 * np.random.rand(1, len(mod)), X, c=col[k-1], label=key)
            pyplot.xlim([3.5, 9.5])
            pyplot.hold(True)
            k += 1
            Y = X.copy()
            Y[Y<0] = 0
            print '{}: {}, {}'.format(key, np.mean(X), np.mean(Y))
    pyplot.legend(fontsize=12)

    k = 0
    pyplot.figure()
    for key in res:
        VCs, mod = zip(*res[key])
        pyplot.scatter(np.array([k]*len(mod)) + 0.6 * np.random.rand(1, len(mod)), np.array(mod), c=col[k-1], label=key)
        pyplot.xlim([-0.5, 12])
        pyplot.hold(True)
        k += 1
    pyplot.legend(fontsize=12)
    k = 0
    for key in res:
        pyplot.figure()
        VCs, mod = zip(*res[key])
        n, bins, patches = pyplot.hist(np.array(mod), bins = 20)
        pyplot.setp(patches, 'facecolor', col[k-1], 'alpha', 0.75)

        pyplot.title(key)
        pyplot.hold(True)
        k += 1
    k = 4
    for key in res:
        if key in AgrMethods:
            pyplot.figure()
            VCs, mod = zip(*res[key])
            X = np.array(mod) - best
            n, bins, patches = pyplot.hist(X, bins = 20)
            pyplot.setp(patches, 'facecolor', col[k-1], 'alpha', 0.75)
            pyplot.title(key)
            pyplot.hold(True)
            k += 1

    pyplot.show()


if __name__ == '__main__':
    G = Benchmarks.MSU_overheard_data()
    draw_real_res()
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    #Data = Benchmarks.ego_vk_data(pool)
    # #drawTests(Data)
    res = []
    test_name = 'ego'
    if test_name == 'ego':
        Data = Benchmarks.ego_vk_data(pool)
        res = calcDist(Data)
    else:
        Data = Benchmarks.internet_data()
        res = calcDist(Data)
        print '\n'.join([key + ': ' + str(res[key][1]) for key in res])
    with file('./result/temp.real', 'wb') as f:
          dump(res, f)

    if test_name == 'ego':
        draw_real_res()
